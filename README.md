**Обсуждение: https://vk.com/topic-158932311_36142985

**Дополнительные ресурсы:**

* [Apache License](http://www.apache.org/licenses/)
* [jrsoftware.org](http://jrsoftware.org/)
* [restools](http://restools.hanzify.org/)
* [Inno Download Plugin](https://bitbucket.org/mitrich_k/inno-download-plugin)
* [VCL-Styles](https://github.com/RRUZ/vcl-styles-plugins)
* [Bass](http://www.un4seen.com/)
* [iCatalyst](https://github.com/lorents17/iCatalyst)

**Автор: Banan_Live.**
**Copyright 2018 The Apache Software Foundation.**
